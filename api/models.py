from django.db import models
from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, email, password):
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(
            email=self.normalize_email(email),
        )
        print(password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user.set_password(password)
        user.save(using=self._db)
        return user


class Users(models.Model):
    email = models.EmailField(
        max_length=128, unique=True, db_index=True, null=True, blank=True)
    password = models.CharField(max_length=128)
    username = models.CharField(max_length=128)
